# v24.51

- Petsc: 3.22.1 -> 3.22.2
- Slepc: 3.22.1 -> 3.22.2
- Boost: 1.86 -> 1.87
- Scotch: 7.0.5 -> 7.0.6
- Openmpi: 5.0.5 -> 5.0.6

# v24.46

- Petsc: 3.21.5 -> 3.22.1
- Slepc: 3.21.1 -> 3.22.1

# v24.41

- Boost: 1.85 -> 1.86
- Lua: 5.4.6 -> 5.4.7
- OpenBLAS: 0.3.26 -> 0.3.28
- Openmpi: 5.0.3 -> 5.0.5
- Petsc: 3.21.2 -> 3.21.5
- Scotch: 7.0.4 -> 7.0.5

- Bug #31: fix SuiteSparse compilation in macOS.
- Bug #32: fix libmeshb compilation with gcc, and few improvements in it (asynchronous mode is restored)

# v24.27

- Feature #30: Add Eigen 3.4.0

# v24.24

- Bug #28: fix Ubuntu Dockerfile which no longer accepted direct `pip` command.
- Bug #29: add a command to fix installation of PETSc 3.21 on macOS / ARM architecture.
- Support #14: Scotch - switch to CMake compilation.

- Boost: 1.84 -> 1.85
- Libmeshb: 7.60 -> 7.80
- Openmpi: 5.0.1 -> 5.0.3
- Scotch: 6.1.2 -> 7.0.4
- Petsc: 3.20.3 -> 3.21.2
- Slepc: 3.20.1 -> 3.21.1
- Xsimd: 12.1.1 -> 13.0.0
- Xtensor: 0.24.7 -> 0.25.0
- XtensorBlas: 0.20.0 -> 0.21.0

# v24.03

- Openmpi: remove an option that was there only for deprecated C++ bindings and that cause an error with Openmpi 5.

- Boost: 1.83 -> 1.84
- Openblas: 0.3.24 -> 0.3.26
- Openmpi: 4.1.6 -> 5.0.1
- Petsc: 3.20.0 -> 3.20.3
- Slepc: 3.20.0 -> 3.20.1
- Xsimd: 11.1.0 -> 12.1.1
- Xtl: 0.7.5 -> 0.7.7

# v23.40

- Support #27: Switch to multi-stage build and copy from option for Docker images.

- Boost: 1.82.0 -> 1.83.0
- Openblas: 0.3.23 -> 0.3.24
- Openmpi: 4.1.5 -> 4.1.6
- Petsc: 3.19.2 -> 3.20
- Slepc: 3.19.1 -> 3.20
- Xtensor: 0.24.6 -> 0.24.7

# v23.24

- Support #25: Docker Ubuntu image: upgrade gcc to gcc 13, and PPA is used to provide a CMake >= 3.24.

- Xsimd: 11.0.0 -> 11.1.0.
- Slepc: 3.19.0 -> 3.19.1.
- Petsc: 3.19.0 -> 3.19.2.
- Lua: 5.4.4 -> 5.4.6.

# v23.17

- Bug #24: Fix the issue that PETSc 3.19 doesn't work on Fedora systems with clang compiler.
- Support #16: Remove six module

- Xtensor: 0.24.5 -> 0.24.6
- Xsimd: 10.0.0 -> 11.0.0
- Openblas: 0.3.21 -> 0.3.23.
- Boost: 1.81 -> 1.82
- Slepc: 3.18.2 -> 3.19
- PETSc: 3.18.5 -> 3.19

# v23.10

- Support #23: Use `tee` command and artefacts to be able to access the log completely when the size limit of ~ 4 Mo is reached
- Bug #22: Change the Docker image used for Docker-in-Docker to fix creation of Ubuntu images.
- Bug #21: Fix openblas installation script to avoid the `make install` issue.
- Feature #20: Add options to choose to install or not some solvers for PETSc.

- Boost: 1.80 -> 1.81.
- Xsimd: 9.1.0 -> 10.0.0.
- Xtensor: 0.24.3 -> 0.24.5
- Xtl: 0.7.4 -> 0.7.5
- Slepc: 3.18.1 -> 3.18.2.
- PETSc: 3.18.1 -> 3.18.5.
- Openmpi: 4.1.4 -> 4.1.5.

# v22.46

- Feature #17: Introduce Slepc library.
- Feature #18: PETSc and Slepc libraries are installed "in place".

- Petsc: 3.17.4 -> 3.18.1
- Xsimd: 8.1.0 -> 9.1.0
- Xtensor: 0.24.2 -> 0.24.3

# v22.36

- Support #13: CI: use newly available shared runners instead of dedicated VMs.
- Bug #15: in PETSc build, force the version of CMake to avoid an issue with clang / gfortran compilation of SuperLU_dist.

- Xsimd: 8.0.5 -> 8.1.0
- Xtensor: 0.24 -> 0.24.4
- Petsc: 3.16.2 -> 3.17.4.
- Openmpi: 4.1.2 -> 4.1.4.
- Openblas: 0.3.15 -> 0.3.21.
- Boost: 1.78 -> 1.80.
- Lua: 5.4.3 -> 5.4.4.


# v22.01

- Support #9: Modify visibility option in Boost 
- Bug #10: The specified compiler in the script was not actually used for Boost; fixed now.
- Support #12: Upgrade to gcc 10 Ubuntu Docker images.
- Support: PETSc: Add 'download-cmake' in PETSc script as the one installed by apt is not a recent enough version.
- Bug #11: PETSc 3.16 doesn't seem to work well on macOS with framework Accelerate. A prompt has been added in the script to warn use about this and encouraging him to use Openblas instead.

- Openblas: 0.3.15 -> 0.3.17
- Openmpi: 4.1.1 -> 4.1.2
- Petsc: 3.14.4 -> 3.16.2
- Xsimd: 7.4.9 -> 8.0.5
- Xtensor: 0.23.10 -> 0.24
- Xtensor-blas: 0.19.1 -> 0.20.0


# v21.27.2

- Bug #8: Remove an option in Openmpi build which broke compilation on case insensitive macOS.

# v21.27

- Petsc: 3.15.0 -> 3.15.1
- Feature #6: Introduce Scotch/PT-Scotch library. The scripts have been extended slightly to do so.
- Feature #5: Remove PCRE and Parmetis.
- Support #7: Libmeshb: add WITH_AIO option to link correctly with librt.


# v21.22

- Libmeshb: 7.56 -> 7.60
- Lua: 5.4.2 -> 5.4.3
- Openblas: 0.3.13 -> 0.3.15
- Openmpi: 4.1.0 -> 4.1.1
- Petsc: 3.14.4 -> 3.15
- Xsimd: 7.4.9 -> 7.5
- Xtensor: 0.23.1 -> 0.23.10
- Xtensor-blas: 0.17.2 -> 0.19.1


# v20.51.2

- Fix a small issue in Fedora Dockerfile that deactivated CMake command.

# v20.51

- Support #3: For Docker build: add NOAVX and NOAVX2 for Openblas.
- Support #4: Add two flags in Openmpi script to circumvent potentiel issue in macOS builds (to force the use of internal hwloc and libevent and thus avoid conflict when compiling with AppleClang and gfortran).
- OpenBLAS: 0.3.9 -> 0.3.10
- Openmpi: 4.0.4 -> 4.0.5
- Petsc: 3.13.2 -> 3.14.2
- Xsimd: 7.4.8 -> 7.4.9
- Xtensor: 0.21.5 -> 0.21.10
- Xtl: 0.6.13 -> 0.6.22


# v20.24

- Support #2: CI: Improve the Yaml file by pushing the latest image tag only if UPDATE_LATEST_TAG is set to True.
- OpenBLAS: 0.3.7 -> 0.3.9
- Openmpi: 4.0.2 -> 4.0.4
- Petsc: 3.12.4 -> 3.13.2
- Xsimd: 7.4.6 -> 7.4.8
- Xtensor: 0.21.3 -> 0.21.5
- Xtensor-blas: 0.17.1 -> 0.17.2
- Xtl: 0.6.12 -> 0.6.13

# v20.10

- Feature #1: Deployment by Gitlab CI/CD, to avoid the hassle of generating all images on my laptop.
- Boost: remove system and filesystem modules.
- Ubuntu Dockerfile now uses up gcc 9, as gcc 8 does not yet implement STL filesystem.
- Update QuantStack stack to more recent versions: Xsimd 7.4.6, Xtensor 0.21.3, Xtensor-blas 0.17.1 and Xtl 0.6.12.
- Update Tclap to v1.2.2.
- Update Openblas to 0.3.7.
- Update PETSc to 3.12.4
- Various fixes in Dockerfiles and Yaml.


# v19.42

- Update Openmpi to v4.0.2.
- Update Petsc to v3.11.4 (v3.12 was attempted but it breaks codes that is used in non linear solve; it is either a bug they have or a change in API poorly documented).
- Update Libmeshb to v7.52.
- Add Tclap library (formerly embedded within MoReFEM sources)
- Add Xtensor, Xtl, Xtenspr-blas and Xsimd (in replacement of Seldon which was embbedded within MoReFEM sources).


# v19.27

- Update Petsc to v3.11.3.
- Update Openblas to 0.3.6
- Revert to Boost 1.67: versions 1.68 and 1.69 get issues (missing symbol in 1.68, and ill-designed modifications in hidden symbol options in 1.69 that breaks runtime of MoReFEM and issue a link warning I didn't manage to solve).
- Update Openmpi to 4.0.1
- Add explicitly -stdlib=libc++ in CXXFLAGS for all libraries to avoid conflicts between libc++ and libstdc++ (the issue was probably here before but led to a compilation error due to a difference of signature in <variant>).
- Separate Fedora Dockerfile to limit the size and not install both compiler (only a half success: clang package installs gcc currently as dependency...)
- Modify the Dockerfile to capitalize on the recent Docker option --squash to reduce image size dramatically.
- Now that Petsc supports Python3, remove all occurrences of Python2 and use the Python3 everywhere (simply named Python).


# v19.16

I've been sloppy and not issued a proper tag, even if Docker images were generated with the tag name in the registry.

# v18.47

- Update Petsc to v3.10.2
- Add the option to choose the mode for libraries: debug, release, or debug with address sanitizer (the latter till being experimental). Formerly all were in release mode except Petsc which was installed with the two flavors.
- Openblas: add instruction not to stop on leak errors
- Remove Scalapack: let Petsc handle it directly.
- Update Dockerfiles.


# v18.40

- Introduce present Changelog file.
- Update Petsc to v3.10.1
- Update Boost to 1.68.
- Remove SuperLU_dist and Mumps: use versions embedded in Petsc, as Petsc devs recommends.
- Remove Yuni.
- Improve multi-platform support for Petsc.
- Update Lua to 5.3.5.
- Update Openmpi to v3.1.2.
- Add a macro definition in Lua compilation to hopefully get rid of a warning that happens at link time about unsafe command.
- Improve Dockerfile.
