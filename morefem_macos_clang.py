
import multiprocessing
import Builder.install_morefem_libraries


if __name__ == "__main__":
    
    mode = "debug"
    target_directory = "/Volumes/Data/opt/clang_{}".format(mode)
    C_compiler = "clang"
    CXX_compiler = "clang++"
    Fortran_compiler = "/usr/local/bin/gfortran"
    blas_library = "Accelerate"
    python = "/Users/Shared/Software/miniconda3/envs/Python3/bin/python"
    boost_toolset = "clang"
    do_prompt_user = True
    do_install_slepc=True

    # Flex and Bison values here are for ARM macOS when they were installed with Homebrew.
    flex = "/opt/homebrew/opt/flex/bin/flex"
    bison = "/opt/homebrew/opt/bison/bin/bison"
    
    Builder.install_morefem_libraries.install_morefem_libraries(target_directory = target_directory, \
                                                                C_compiler = C_compiler, \
                                                                CXX_compiler = CXX_compiler, \
                                                                python = python, \
                                                                Fortran_compiler = Fortran_compiler, \
                                                                mode = mode, \
                                                                blas_library = blas_library, \
                                                                do_use_fPIC = False, \
                                                                Nproc = multiprocessing.cpu_count(), \
                                                                boost_toolset = boost_toolset,
                                                                do_prompt_user = do_prompt_user,
                                                                do_install_slepc=do_install_slepc,
                                                                flex = flex,
                                                                bison = bison)