
    
    
class PetscExternalPackages:

    def __init__(self, petsc_do_download_mumps, petsc_do_download_scalapack, petsc_do_download_superlu_dist, petsc_do_download_umfpack):
        """
        @param petsc_do_download_mumps If true, PETSc installation script will download and install Mumps.
        @param petsc_do_download_umfpack If true, PETSc installation script will download and install SuiteSparse (Which includes Umfpack).
        @param petsc_do_download_superlu_dist If true, PETSc installation script will download and install SuperLU_dist.
        @param petsc_do_download_scalapack If true, PETSc installation script will download and install Scalapack.
        """
        self._do_download_mumps = petsc_do_download_mumps
        self._do_download_scalapack = petsc_do_download_scalapack
        self._do_download_superlu_dist = petsc_do_download_superlu_dist
        self._do_download_umfpack = petsc_do_download_umfpack


    @property
    def do_download_mumps(self):
        return self._do_download_mumps

    @property
    def do_download_scalapack(self):
        return self._do_download_scalapack

    @property
    def do_download_superlu_dist(self):
        return self._do_download_superlu_dist

    @property
    def do_download_umfpack(self):
        return self._do_download_umfpack
    
    def _do_install_mumps(self, line):
        """
        Line that is to be modified if it contains '[M3DISIM_PETSC_DOWNLOAD_MUMPS]'
        """
        download_command = ""

        if self.do_download_mumps:
            download_command = "--download-mumps"

        line = line.replace("[M3DISIM_PETSC_DOWNLOAD_MUMPS]", download_command)

        return line


    def _do_install_umfpack(self, line):
        """
        Line that is to be modified if it contains '[M3DISIM_PETSC_DOWNLOAD_UMFPACK]'
        """
        download_command = ""

        if self.do_download_umfpack:
            download_command = "--download-suitesparse"

        line = line.replace("[M3DISIM_PETSC_DOWNLOAD_UMFPACK]", download_command)

        return line


    def _do_install_superlu_dist(self, line):
        """
        Line that is to be modified if it contains '[M3DISIM_PETSC_DOWNLOAD_SUPERLU_DIST]'
        """
        download_command = ""

        if self.do_download_superlu_dist:
            download_command = "--download-superlu_dist"

        line = line.replace("[M3DISIM_PETSC_DOWNLOAD_SUPERLU_DIST]", download_command)

        return line


    def _do_install_scalapack(self, line):
        """
        Line that is to be modified if it contains '[M3DISIM_PETSC_DOWNLOAD_SCALAPACK]'
        """
        download_command = ""

        if self.do_download_scalapack:
            download_command = "--download-scalapack --download-scalapack-cmake-arguments='-DCMAKE_C_FLAGS:STRING=-Wno-implicit-function-declaration'"

        line = line.replace("[M3DISIM_PETSC_DOWNLOAD_SCALAPACK]", download_command)

        return line

    def substitute_download_commands(self, line):
        """
        Analyze line and substitute in it by the correct settings the eventual download commands for PETSc external packages.
        """
        line = self._do_install_mumps(line)
        line = self._do_install_scalapack(line)
        line = self._do_install_superlu_dist(line)
        line = self._do_install_umfpack(line)
        return line
