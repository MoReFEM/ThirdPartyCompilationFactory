import subprocess
import os
import platform


class Compiler(object):
    
    def __init__(self, C, CXX, Fortran, python, blas_library, do_use_fPIC, boost_toolset, mex = None):
        """
        @param python Python 3 executable.
        """
        self._C = C
        self._CXX = CXX   
        self._Fortran = Fortran     
        self._do_use_fPIC = do_use_fPIC
        self._are_mpi_compilers_defined = False
        self._python = python
        self.__mex = False
        self.__boost_toolset = boost_toolset
        
        if mex:
            self.__mex = mex
        
        for language in (C, CXX, Fortran, python):
            if not subprocess.Popen(["which", language], stdout=subprocess.PIPE).communicate()[0]:
                raise Exception("{0} was not found!".format(language))
        
        if "clang++" in self.CXX:
            self._stdlib = "-stdlib=libc++"
            self._stllinker = "{} -lc++".format(self._stdlib)
        elif "g++" in self.CXX:
            self._stdlib = ""
            self._stllinker = ""
        else:
            raise Exception("No default STL linker could be provided for compiler {compiler}; please specify it in constructor.".format(compiler = self.CXX))
                
        
        supported_blas_lib = ("Openblas", "Accelerate")
        
        if blas_library not in supported_blas_lib:
            raise Exception("Blas library '{0}' is not supported; valid choices are: {1}".format(blas_library, supported_blas_lib))
            
        self._blas_library = blas_library
        
        # Find compilation informations, to be written in each M3DISIM_build.txt file.
        cmd = (C, "-v")
        check = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()     
        self._info = check[1].decode('utf8') # For some reason informations are driven to stderr for both gcc and clang.
        
        cmd = (Fortran, "-v")
        check = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()     
        self._info = "{0} \n\nand\n\n {1}".format(self._info, check[1].decode('utf8')) # For some reason informations are driven to stderr for both gcc and clang.
        
    @property
    def C(self):
        return self._C   
        
    @property
    def CXX(self):
        return self._CXX
        
    @property
    def stdlib(self):
        return self._stdlib
        
    @property
    def stllinker(self):
        return self._stllinker
        
    @property
    def Fortran(self):
        return self._Fortran
        
    @property
    def boost_toolset(self):
        return self.__boost_toolset    
        
    @property
    def blas_library(self):
        return self._blas_library        
        
    @property
    def mpiC(self):
        assert(self.AreMpiCompilersDefined())        
        return self.__mpiC
        
    @property
    def Python(self):
        return self._python
        
    @property
    def mpiCXX(self):
        assert(self.AreMpiCompilersDefined())        
        return self.__mpiCXX
        
    @property
    def mpiFortran(self):
        assert(self.AreMpiCompilersDefined())        
        return self.__mpiFortran
        
    @property
    def mex(self):
        return self.__mex
        
    @property
    def info(self):
        return self._info
        
    def BlasLinker(self):
        if self._blas_library == "Accelerate":
            return "-framework Accelerate"
        else:
            if platform.system() == "Linux":
                return "-L[M3DISIM_INSTALL_LIBRARY_DIRECTORY]/Openblas/lib -Wl,-rpath=[M3DISIM_INSTALL_LIBRARY_DIRECTORY]/Openblas/lib -lopenblas"
            else:
                return "-L[M3DISIM_INSTALL_LIBRARY_DIRECTORY]/Openblas/lib -lopenblas"
        
    def AreMpiCompilersDefined(self):
        return self._are_mpi_compilers_defined
                
    def SetMpiCompilers(self, mpi_dir):
        self._are_mpi_compilers_defined = True
        self.__mpiC = os.path.join(mpi_dir, "mpicc")
        self.__mpiCXX = os.path.join(mpi_dir, "mpicxx")        
        self.__mpiFortran = os.path.join(mpi_dir, "mpifort")
        
        
    def fPICFlags(self):
        if self._do_use_fPIC:
            return "-fPIC"
        else:
            return ""
        
    def PositionIndependentCode(self):
        if self._do_use_fPIC:
            return "-DCMAKE_POSITION_INDEPENDENT_CODE=True"
        else:
            return ""