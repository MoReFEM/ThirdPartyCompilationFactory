import os
import shutil

from .compiler import Compiler
from .build_third_party_libraries import ThirdPartyBuild
from .petsc_external_packages import PetscExternalPackages


def docker_build(target_directory, C_compiler, CXX_compiler, Fortran_compiler, python, blas_library, do_use_fPIC, boost_toolset, mode, do_install_all_supported_petsc_solvers, Nproc = 4):

    build_third_party_libraries_directory = os.getcwd()

    compiler = Compiler(C = C_compiler, CXX = CXX_compiler, Fortran = Fortran_compiler, blas_library = blas_library, do_use_fPIC = do_use_fPIC, python = python, mex = None, boost_toolset = boost_toolset)


    # Note: at the moment SuperLU_dist is the de facto default solver in MoReFEM and is always installed.
    petsc_external_packages = PetscExternalPackages(petsc_do_download_mumps = do_install_all_supported_petsc_solvers, \
                                                    petsc_do_download_scalapack = do_install_all_supported_petsc_solvers, \
                                                    petsc_do_download_superlu_dist = True, \
                                                    petsc_do_download_umfpack = do_install_all_supported_petsc_solvers)
    
    
    bison = shutil.which("bison")

    if not bison:
        raise Exception("`bison` executable not found in your PATH! Check it was properly installed in the Docker image.")
    
    flex = shutil.which("flex")

    if not flex:
        raise Exception("`flex` executable not found in your PATH! Check it was properly installed in the Docker image.")

    third_party_build = ThirdPartyBuild(compiler, build_third_party_libraries_directory, target_directory = target_directory, mode = mode, Nproc = Nproc, do_prompt_user = False, petsc_external_packages = petsc_external_packages, is_docker_build = True, flex = flex, bison = bison)
    
    third_party_build.SetMpiCompilers()
    
    script_list = []
    
    if compiler.blas_library == "Openblas":
        script_list.append(third_party_build.SetUpLibrary("Openblas"))

    script_list.append(third_party_build.SetUpLibrary("Lua"))
    script_list.append(third_party_build.SetUpLibrary("Tclap"))    
    script_list.append(third_party_build.SetUpLibrary("Libmeshb"))
    script_list.append(third_party_build.SetUpLibrary("Boost"))
    script_list.append(third_party_build.SetUpLibrary("Openmpi"))
    script_list.append(third_party_build.SetUpLibrary("Scotch"))
    script_list.append(third_party_build.SetUpLibrary("Petsc"))
    script_list.append(third_party_build.SetUpLibrary("Slepc"))
    script_list.append(third_party_build.SetUpLibrary("Eigen"))
    script_list.append(third_party_build.SetUpLibrary("Xtl"))
    script_list.append(third_party_build.SetUpLibrary("Xsimd"))
    script_list.append(third_party_build.SetUpLibrary("Xtensor"))
    script_list.append(third_party_build.SetUpLibrary("XtensorBlas"))
    
    print("The following scripts should be run in Docker file with RUN:")
    
    for item in script_list:
        print(item)