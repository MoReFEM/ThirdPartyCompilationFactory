import os
import glob
import shutil
import subprocess
import tarfile
import zipfile
import datetime
import platform

from .compiler import Compiler


class ThirdPartyBuild(object):
    
    
    def __init__(self, compiler, build_third_party_helper_directory, target_directory, Nproc, mode, do_prompt_user, petsc_external_packages, flex, bison, is_docker_build = False):
        """
        @param compiler A compiler object which describes C, C++ and Fortran compiler, the STL used, whether mpi wrappers are used and so forth...
        @param build_third_party_helper_directory The directory in which the current script is present.
        @param target_directory Directory into which the libraries and includes required to use the libraries will be put. If not existent, it is created on the fly.
        @param Nproc Number of processors available for the build.     
        @param do_prompt_user If True, user will be asked for each library if he wants it installed. If False it is assumed all libraries must be installed.  
        @param is_docker_build Specifies whether the build is intended for a Docker image. This setting is useful
        to disable in this case settings in builds that rely upon underlying architecture (Openblas for instance could
        crash in runtime with a 'normal' build).
        @param petsc_external_packages Object which tells whether some external PETSc packages must be installed or not.
        @param flex Path to flex executable.
        @param bison Path to bison executable.
        """
        
        # Directory into which the tarballs are extracted and from which installation scripts are played. The point is to be able to remove it once build is done: the meaningful parts are assumed to be put in the target_directory. f not existent, it is created on the fly.
        self._temporary_build_directory = "/tmp/morefem_install_3rd_party"
        
        if not os.path.exists(self._temporary_build_directory):
            os.makedirs(self._temporary_build_directory)
            
        if not subprocess.Popen(["which", "cmake"], stdout=subprocess.PIPE).communicate()[0]:
            raise Exception("cmake was not found; please install it first and make it available in your $PATH!")
    
        if not os.path.exists(target_directory):
            os.makedirs(target_directory)
        
        self._compiler = compiler
        self._build_third_party_helper_directory = build_third_party_helper_directory
    
        self._target_directory = target_directory
    
        self._Nproc = Nproc
        self._do_prompt_user = do_prompt_user
        self._petsc_external_packages = petsc_external_packages

        self._flex = flex
        self._bison = bison

        # From http://stackoverflow.com/questions/5891555/display-the-date-like-may-5th-using-pythons-strftime
        def suffix(d):
            return 'th' if 11 <= d <= 13 else { 1:'st', 2:'nd', 3:'rd'}.get(d % 10, 'th')

        def custom_strftime(format, t):
            return t.strftime(format).replace('{S}', str(t.day) + suffix(t.day))

        self._current_day = custom_strftime('{S} of %B %Y', datetime.datetime.now())
        
        self._mode = mode
    
        self.SetMpiCompilers()

        self._is_docker_build = is_docker_build
        
        self._was_xtl_set = False
        

    def InterpretFile(self, filename, library_directory, library_name, is_in_place_build):
        """Read a file from CompilationData/*library* directory and rewrite it after substituting data relevant for current build (for instance [M3DISIM_INSTALL_DATE] by the actual date). It may also return the list of commands found in the file, a command being defined as a line beginning by '$'. It is useful when this method is used on a M3DISIM_build.txt file.
        
        @param filename Name of the file being interpreted (and resulting file will bear the same name).
        @param library_directory Subdirectory of CompilationData which contains the library being processed.
        @param library_name Name of the library,in M3DISIM convention. So 'Lua', 'Openmpi', 'Petsc', etc...
        @param is_docker_build Specifies whether the build is intended for a Docker image. This setting is useful
        to disable in this case settings in builds that rely upon underlying architecture (Openblas for instance could
        crash in runtime with a 'normal' build).
        
        \return List of commands to be run in a terminal. All lines that begins by a '$' are considered to be run
        in terminal and added to this list (with the $ dropped).
        """
        build_directory = self._SetBuildDirectory(is_in_place_build)

        os.chdir(build_directory)
        FILE_in = open(os.path.join(library_directory, filename))        
        
        work_directory = build_directory
        
        FILE_out = open(os.path.join(work_directory, library_name, filename), "w")
     
        ret = []
        
        compiler = self._compiler
        
        # First read the file and store each line in a list.
        # Aggregate two lines if the former ends by '\'.
        file_content = []
        buf_line_list = []
        
        do_skip = False
        
        for line in FILE_in:
            line = line.strip('\n\r\t')
            line = line.lstrip(' ')
            
            if line.startswith("[BEGIN_M3DISIM_INSTALL_OPENBLAS]"):
                if compiler.blas_library != "Openblas":
                    do_skip = True
                continue
                
            if line.startswith("[END_M3DISIM_INSTALL_OPENBLAS]"):    
                do_skip = False
                continue
                
            if do_skip:
                continue
            
            if line.endswith('\\'):
                buf_line_list.append("{0}[M3DISIM_INSTALL_ANTISLASH]".format(line[:-1]))
            else:
                buf_line_list.append(line)
                file_content.append(" ".join(buf_line_list))
                buf_line_list = []
                
        if buf_line_list:
            file_content.append(" ".join(buf_line_list))
            
        
        openblas_docker = self._is_docker_build and "DYNAMIC_ARCH=1 NO_AFFINITY=1 NUM_THREADS=32 NO_AVX2=1  NO_AVX=1" or ""
            
        for line in file_content:
            line = line.replace("[M3DISIM_INSTALL_BLAS]", compiler.BlasLinker()) # Must be before M3DISIM_INSTALL_LIBRARY_DIRECTORY         
            line = line.replace("[M3DISIM_INSTALL_C_COMPILER]", compiler.C)
            line = line.replace("[M3DISIM_INSTALL_CXX_COMPILER]", compiler.CXX)
            line = line.replace("[M3DISIM_INSTALL_FORTRAN_COMPILER]", compiler.Fortran)
            line = line.replace("[M3DISIM_INSTALL_COMPILER_INFO]", compiler.info)
            line = line.replace("[M3DISIM_INSTALL_LIBRARY_DIRECTORY]", self._target_directory)
            line = line.replace("[M3DISIM_INSTALL_HELPER_LIBRARY_DIRECTORY]", self._temporary_build_directory)            
            line = line.replace("[M3DISIM_INSTALL_DATE]", self._current_day)
            line = line.replace("[M3DISIM_INSTALL_FPIC]", compiler.fPICFlags())
            line = line.replace("[M3DISIM_INSTALL_CMAKE_POSITION_INDEPENDENT_CODE]", compiler.PositionIndependentCode())
            line = line.replace("[M3DISIM_INSTALL_NPROCESSOR]", str(self._Nproc))
            line = line.replace("[M3DISIM_INSTALL_STLLIB]", compiler.stdlib)
            line = line.replace("[M3DISIM_INSTALL_STL_LINKER]", compiler.stllinker)
            line = line.replace("[M3DISIM_INSTALL_BOOST_TOOLSET]", compiler.boost_toolset)
            line = line.replace("[M3DISIM_INSTALL_PYTHON]", compiler.Python)

            line = line.replace("[M3DISIM_INSTALL_FLEX_EXE]", self._flex)
            line = line.replace("[M3DISIM_INSTALL_BISON_EXE]", self._bison)
            
            line = line.replace("[M3DISIM_INSTALL_OPENBLAS_DOCKER]", openblas_docker)

            line = self._petsc_external_packages.substitute_download_commands(line)
            
            if self._mode == 'release':                
                line = line.replace("[M3DISIM_INSTALL_MODE_FLAGS]", "-O3 -DNDEBUG")
                line = line.replace("[M3DISIM_INSTALL_MODE]", "release")
                line = line.replace("[M3DISIM_INSTALL_MODE_UPPERCASE]", "Release")    
                line = line.replace("[M3DISIM_INSTALL_SANITIZER_FLAGS]", "") 
                line = line.replace("[M3DISIM_INSTALL_OPENBLAS]", "")   
                line = line.replace("[M3DISIM_PETSC_IS_DEBUGGING]", "no")
            elif self._mode == 'debug_with_asan':                
                if platform.system() == "Darwin":
                    raise Exception("Address sanitizer not supported on macOS (not activated for the installed gfortran).")
                
                line = line.replace("[M3DISIM_INSTALL_MODE_FLAGS]", "-g ")
                line = line.replace("[M3DISIM_INSTALL_MODE]", "debug")
                line = line.replace("[M3DISIM_INSTALL_MODE_UPPERCASE]", "Debug")
                line = line.replace("[M3DISIM_INSTALL_SANITIZER_FLAGS]", "-fno-omit-frame-pointer -fsanitize=address -fsanitize-recover=address") 
                line = line.replace("[M3DISIM_INSTALL_OPENBLAS]", "DEBUG=1")                         
                line = line.replace("[M3DISIM_PETSC_IS_DEBUGGING]", "yes")                
            elif self._mode == 'debug':
                line = line.replace("[M3DISIM_INSTALL_MODE_FLAGS]", "-g ")
                line = line.replace("[M3DISIM_INSTALL_MODE]", "debug")
                line = line.replace("[M3DISIM_INSTALL_MODE_UPPERCASE]", "Debug")
                line = line.replace("[M3DISIM_INSTALL_SANITIZER_FLAGS]", "") 
                line = line.replace("[M3DISIM_INSTALL_OPENBLAS]", "DEBUG=1")                         
                line = line.replace("[M3DISIM_PETSC_IS_DEBUGGING]", "yes")
            else:
                raise Exception("Invalid mode: only 'release', 'debug' and 'debug_with_asan' are allowed.")

            if "gcc" in compiler.C:
                line = line.replace("[M3DISIM_INSTALL_GOMP]", "-lgomp")
            else:
                line = line.replace("[M3DISIM_INSTALL_GOMP]", "")
            
            if compiler.mex:
                line = line.replace("[M3DISIM_INSTALL_MEX]", compiler.mex)
            elif "[M3DISIM_INSTALL_MEX]" in line:
                raise Exception("You need to specify mex path in constructor of the main if you want to compile this one!")
            
            if compiler.AreMpiCompilersDefined():
                line = line.replace("[M3DISIM_INSTALL_MPI_C_COMPILER]", compiler.mpiC)
                line = line.replace("[M3DISIM_INSTALL_MPI_CXX_COMPILER]", compiler.mpiCXX)
                line = line.replace("[M3DISIM_INSTALL_MPI_FORTRAN_COMPILER]", compiler.mpiFortran)
            else:
                print("Mpi compiler not defined!")

            # See #29: using this option was the only way to make PETSc 3.21 compile.
            if platform.system() == "Darwin" and platform.machine() == "arm64":
                line = line.replace("[M3DISIM_INSTALL_NO_COMPACT_UNWIND_FOR_ARM]", "-Wl,-no_compact_unwind")
            else:
                line = line.replace("[M3DISIM_INSTALL_NO_COMPACT_UNWIND_FOR_ARM]", "")
            
            line_for_cmd_list = line.lstrip()
            
            # If it is a command, add it to the list of commands.
            if line_for_cmd_list.startswith('$'):
                line_for_cmd_list = line_for_cmd_list.replace("[M3DISIM_INSTALL_ANTISLASH]", " ")  
                ret.append(line_for_cmd_list[1:].strip('\t\n\r'))
                
            # In any case, report the line in the target M3DISIM_build.txt file.
            line = line.replace("[M3DISIM_INSTALL_ANTISLASH]", "\\ \n")    
            FILE_out.write("{0}\n".format(line))
        
        return ret
        
        
    def Uncompress(self, target_directory, tarball_filename, library_name):
        """
        Uncompress the tarball present in CompilationData directory.
        
        @param target_directory Directory in which the library must be installed.
        @param tarball_filename Name of the tarball to be uncompressed.
        @param library_name Name of the library considered; the output directory will use this name.
        """
        new_library_path = os.path.join(target_directory, library_name)

        if os.path.exists(new_library_path):
            print("[WARNING] Removing pre-existing {0} directory.".format(new_library_path))
            shutil.rmtree(new_library_path)
        
        os.chdir(target_directory)
        
        if tarball_filename.endswith(".zip"):
            archive = zipfile.ZipFile(tarball_filename, "r")
            archive.extractall()
            directory_name = archive.namelist()[0]
        else:
            archive = tarfile.open(tarball_filename, "r:gz")
            archive.extractall()
            directory_name = archive.getnames()[0]
        
        # I assume here a whole directory was compressed; so first item of the list should be the name of this directory.        
        archive.close()
        os.remove(tarball_filename)

        # Rename: we want to use the name we chose, not the name used by the creators of the original package.
        os.rename(directory_name, new_library_path)
        
    
    def FindTarball(self, library_directory):
        """Find the sole file with ".tar.gz"extension in \a library_directory."""
        
        file_list = os.listdir(library_directory)
    
        tarball_list = [item for item in file_list if item.endswith(".tar.gz") or item.endswith(".tgz") or item.endswith(".zip")]
    
        if len(tarball_list) != 1:
            raise Exception("Only one tarball should be present in {0}".format(library_directory))
        
        return tarball_list[0]
        
        
    def SetUpScript(self, target_library_directory, library_name, command_list, subdirectory_for_make = None):
        """
        SetUp a script file which will run all the commands found in the M3DISIM_build.txt file for the current library.
        
        @param target_library_directory Path to the directory into which the code of the library is installed.
        @param library_name Name of the library for which script is created. It is used to name the script.
        @param command_list All the commands to be written into the script; lifted from the M3DISIM_build.txt.
        @param subdirectory_for_make If not None, the script to build the library will first place itself within a subbdirectory (for instance Scotch must be run from `src` directory).
        
        Note: A hack is there to ensure a failure stops the whole installation procedure, including in a Docker build.
        
        \return Path to the created script.       
        """ 
        script_name = os.path.join(target_library_directory, \
                                  "m3disim_build_{}.sh".format(library_name))

        fd = os.open(script_name, os.O_WRONLY | os.O_CREAT, 0o700)
        stream = os.fdopen(fd, "w")
        
        stream.write("#!/bin/sh\n\n")
        
        exit_trick = "|| exit $?" # See MarcH's comment in https://stackoverflow.com/questions/90418/exit-shell-script-based-on-process-exit-code
        
        stream.write("cd {} {}\n".format(target_library_directory, exit_trick))

        if subdirectory_for_make:
            stream.write("cd {} {}\n".format(subdirectory_for_make, exit_trick))
        
        for cmd in command_list:
            stream.write("{} {}\n".format(cmd, exit_trick))
            
        stream.write("cd {} {}\n".format(self._build_third_party_helper_directory, exit_trick))            
        
        return script_name
  

    def _CheckInstallLibrary(self, library_name, specific_prompt_text = None):
        """
        If self._do_prompt_user is True, just return True.
        If not, ask the user whether he wants to proceed, and returns his answer.
        
        @param library_name Name of the library to be installed (or not!)
        @param specific_prompt_text If None, default question is asked. If not, user may specify here in a string the question to ask. In this case, it supersedes entirely the question asked by default.
        
        \return True if the library must be installed.
        """
        if not self._do_prompt_user:
            return True
            
        print("======================================================")
        if not specific_prompt_text:
            print("Do you want to (re)install library {library_name}? [y/n]".format(library_name = library_name))
        else:
            print(specific_prompt_text)
        print("======================================================")
    
        my_input = input()
    
        while my_input not in ("y", "n"):
            my_input = input()
            
        if my_input == "y":
            return True
            
        return False
        
    def _SetBuildDirectory(self, is_in_place_build):
        """
        Return the build directory to use, which depends whether we do a in place build or not.
        """

        if is_in_place_build:
            return self._target_directory
        else:
            return self._temporary_build_directory


    def _StandardSetUp(self, library_name, file_to_edit_list = [], build_file='M3DISIM_build.txt', specific_prompt_text = None, remove_pre_existing_target_directory = True, subdirectory_for_make = None, is_in_place_build = False):
        """Standard set up procedure for a library: content of the CompilationData folder is copied into the target
        directory, and the files which content depend on the installation are properly written there. At last, a new 
        script file to run to install the library is written; it is not yet run at this level.
        
        @param file_to_edit_list List of files to edit before running the computation. These files include some
        macros to be replaced by proper content: e.g. [M3DISIM_INSTALL_C_COMPILER] must be replaced by the appropriate
        C compiler.
        @param library_name Name of the library being set up.
        @param build_file Name of the text file that gives away the instructions to build the library. Usually M3DISIM_build.txt, but for some libraries different files must be foreseen to account for different compiler and/or OS.
        @param specific_prompt_text Relevant only if self._do_prompt_user is true. If specific_prompt_text is None, default question is asked. If not, user may specify here in a string the question to ask. In this case, it supersedes entirely the question asked by default.
        @param remove_pre_existing_target_directory If True, remove any target directory that already shares the same name. Choose 'True' in almost any cases - 'False' was introduced for the specific case of QuantStack libraries, which are built within the same target.
        @param subdirectory_for_make If not None, the script to build the library will first place itself within a subdirectory (for instance Scotch must be run from `src` directory).
        @param is_in_place_build For most of the libraries, the sources are unpacked in a temporary directory and after compilation something as `make install` is called to install properly only the relevant stuff (headers, libraries, etc...) without keeping all the sources. However, Petsc and Slepc may behave strangely following that (see issue #18 for more about it) so it's nice to be able to build them in place in the target directory; to do so choose `True` for this option (`False` being the default).
        
        \return Path to the installation script, or None of if it is not to be installed.
        """             
        if not self._CheckInstallLibrary(library_name, specific_prompt_text):
            return None
        
        # Find tarball.
        library_directory = os.path.join(self._build_third_party_helper_directory, "CompilationData")        
        
        library_directory = os.path.join(library_directory, library_name)
        
        # Determine target directory in which library is built.
        target_directory = self._SetBuildDirectory(is_in_place_build)
            
        tarball_filename = self.FindTarball(library_directory)

        # Remove any pre-existing target.
        target_dir_for_lib = os.path.join(self._target_directory, library_name)
       
        if remove_pre_existing_target_directory and os.path.exists(target_dir_for_lib):
            print("[WARNING] Removing pre-existing {0} target directory.".format(target_dir_for_lib))
            shutil.rmtree(target_dir_for_lib)
    
        # Uncompress the tarball and rename the obtained directory.
        new_tarball_filename =  os.path.join(target_directory, tarball_filename)
        
        shutil.copyfile(os.path.join(library_directory, tarball_filename), new_tarball_filename)   
        self.Uncompress(target_directory, new_tarball_filename, library_name)
        
        # Generate the correct M3DISIM build file and extract from it the commands to run in terminal.
        target_lib_dir = os.path.join(self._target_directory, library_name)
        
        if not is_in_place_build and not os.path.exists(target_lib_dir):
            os.makedirs(target_lib_dir)

        command_list = self.InterpretFile(build_file,
                                          library_directory,
                                          library_name,
                                          is_in_place_build)

        for item in file_to_edit_list:
            self.InterpretFile(item, library_directory, library_name, is_in_place_build)
                                                
        # SetUp the installation script for the library.
        return self.SetUpScript(os.path.join(target_directory, library_name), library_name, command_list, subdirectory_for_make=subdirectory_for_make)
        
        
    def _StandardBuild(self, script_name):
        """
        Run the script created in SetUpScript().
        
        @param script_name Output of SetUpScript().
        
        In case of failure, an exception is raised.
        """
        print("===========================\nExecuting {}\n===========================\n".format(script_name))
        process = subprocess.Popen(script_name, shell = False, stdout = subprocess.PIPE, stderr = subprocess.PIPE)        
        process.communicate()

        if process.returncode:
            raise Exception("Process {} failed and returned code {}".format(script_name, process.returncode))
          
          
    def _StandardCopy(self, library_name):
        """
        Case for a header only library: the content is copied but nothing more is done.
        """
        os.chdir(self._temporary_build_directory)
    
        library_directory = os.path.join(self._build_third_party_helper_directory, "CompilationData", library_name)
    
        if os.path.exists(library_name):
            print("[WARNING] Removing pre-existing {0} directory.".format(library_name))
            shutil.rmtree(library_name)
    
        shutil.copytree(library_directory, library_name)
        

    def SetUpLua(self):
        
        if platform.system() == "Linux":
            system = "linux"
        elif platform.system() == "Darwin":
            system = "macos"
        else:
            raise Exception("Unknown platform; don't know how to compile Lua.")
        
        return self._StandardSetUp("Lua", build_file='M3DISIM_build_{}.txt'.format(system))


    def BuildLibrary(self, library_name):
        
        script_name = getattr(self, "SetUp{}".format(library_name))
        script = script_name()

        # Script might be None if the user chose not to install the library.
        if script:
            self._StandardBuild(script)      
            return True

        return False
        
        
    def SetUpOpenmpi(self):
        return self._StandardSetUp("Openmpi")
        
    def SetMpiCompilers(self):
        compiler = self._compiler
        mpi_dir = os.path.join(self._target_directory, "Openmpi", "bin")        
        compiler.SetMpiCompilers(mpi_dir)
        
    def SetUpScotch(self):
        return self._StandardSetUp("Scotch")


    def SetUpLibmeshb(self):
        return self._StandardSetUp("Libmeshb")
        
    def SetUpBoost(self):            
        
        return self._StandardSetUp("Boost", 
                                   build_file='M3DISIM_build_{}.txt'.format(self._compiler.boost_toolset), 
                                   specific_prompt_text = "Do you want to (re)install Boost Test library? [y/n]",
                                   file_to_edit_list = ["user-config.jam"])             
        
    def SetUpSuperlu_dist(self):        
        return self._StandardSetUp("Superlu_dist")
        
    def SetUpScalapack(self):        
        if self._compiler.blas_library != "Openblas":
            return self._StandardSetUp("Scalapack")
        else:
            return self._StandardSetUp("Scalapack", build_file='M3DISIM_build_openblas.txt')
        
    def SetUpPetsc(self):
        if self._compiler.blas_library != "Openblas":
            ret = self._StandardSetUp("Petsc", is_in_place_build=True)
        else:
            ret = self._StandardSetUp("Petsc", is_in_place_build=True, build_file='M3DISIM_build_openblas.txt')

        return ret

    def SetUpSlepc(self):
        petsc_dir = "{}/Petsc".format(self._target_directory)

        ret = self._StandardSetUp("Slepc", is_in_place_build=True)

        if ret and not os.path.exists(petsc_dir):
            raise Exception("Slepc requires that PETSc is present at {} and has been installed with the requested configuration. Please launch again the installation script and require this time to install PETSc as well.".format(petsc_dir))

        return ret
            
    def SetUpOpenblas(self):        
        return self._StandardSetUp("Openblas")    
    
    def SetUpEigen(self):
        return self._StandardSetUp("Eigen")
                
    def SetUpXtensor(self):
        assert(self._was_xtl_set)
        return self._StandardSetUp("Xtensor", remove_pre_existing_target_directory=False)    
        
    def SetUpXtensorBlas(self):
        assert(self._was_xtl_set)
        return self._StandardSetUp("XtensorBlas", remove_pre_existing_target_directory=False)
        
    def SetUpXtl(self):
        self._was_xtl_set = True
        return self._StandardSetUp("Xtl")

    def SetUpXsimd(self):
        assert(self._was_xtl_set)
        return self._StandardSetUp("Xsimd", remove_pre_existing_target_directory=False)
        
    def SetUpTclap(self):
        return self._StandardSetUp("Tclap")
        
    def SetUpLibrary(self, library_name):
        """Convenient call to SetUpXXX where XXX is \a library_name)."""
        script_name = getattr(self, "SetUp{}".format(library_name))
        script = script_name()
        
        return script
        

    
    

    
    