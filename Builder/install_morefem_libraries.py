import os
import shutil

from .compiler import Compiler
from .build_third_party_libraries import ThirdPartyBuild
from .petsc_external_packages import PetscExternalPackages


def install_morefem_libraries(target_directory, C_compiler, CXX_compiler, Fortran_compiler, python, mode, blas_library, do_use_fPIC, boost_toolset, do_prompt_user, do_install_slepc, flex = None, bison = None, petsc_do_download_mumps = True, petsc_do_download_scalapack = True, petsc_do_download_superlu_dist = True, petsc_do_download_umfpack = True, Nproc = 8):
    """
    Install all third party libraries required by MoReFEM.
    
    @param target_directory Directory into which the libraries and includes required to use the libraries will be put. If not existent, it is created on the fly.
    @param C_compiler Path to the C compiler to use. Do not put the mpi wrappers here.
    @param CXX_compiler Path to the C++ compiler to use. Do not put the mpi wrappers here.    
    @param Fortran_compiler Path to the Fortran compiler to use. Do not put the mpi wrappers here.
    @param python Path to Python 3.
    @param mode 'debug', 'debug_with_san' (for address sanitizer activated), 'release'.
    @param blas_library Currently "Openblas" and "Accelerate" are supported.
    @param do_use_fPIC Whether -fPIC should be added in compilation.
    @param boost_toolset What is expected in the toolset argument in Boost command line. 'clang' or 'gcc' for instance.
    @param do_prompt_user If True, user will be asked for each library if he wants it installed. If False it is assumed all libraries must be installed.  
    @param do_install_slepc Whether Slepc is to be installed or not.
    @param Nproc Number of processors available for compilation.
    @param petsc_do_download_mumps If true, PETSc installation script will download and install Mumps.
    @param petsc_do_download_umfpack If true, PETSc installation script will download and install SuiteSparse (Which includes Umfpack).
    @param petsc_do_download_superlu_dist If true, PETSc installation script will download and install SuperLU_dist.
    @param petsc_do_download_scalapack If true, PETSc installation script will download and install Scalapack.
    @param flex Path to flex executable. If None, value from `shutil.which` will be used.
    @param bison Path to bison executable. If None, value from `shutil.which` will be used.
    """
    build_third_party_libraries_directory = os.getcwd()

    compiler = Compiler(C = C_compiler, CXX = CXX_compiler, Fortran = Fortran_compiler, blas_library = blas_library, do_use_fPIC = do_use_fPIC, python = python,  mex = None, boost_toolset = boost_toolset)
    
    petsc_external_packages = PetscExternalPackages(petsc_do_download_mumps = petsc_do_download_mumps, \
                                                    petsc_do_download_scalapack = petsc_do_download_scalapack, \
                                                    petsc_do_download_superlu_dist = petsc_do_download_superlu_dist, \
                                                    petsc_do_download_umfpack = petsc_do_download_umfpack)
    
    if not bison:
        bison = shutil.which("bison")

        if not bison:
            raise Exception("`bison` executable not found in your PATH! Check it was properly installed in the Docker image.")
        
    if not flex:
        flex = shutil.which("flex")

        if not bison:
            raise Exception("`flex` executable not found in your PATH! Check it was properly installed in the Docker image.")


    third_party_build = ThirdPartyBuild(compiler, build_third_party_libraries_directory, target_directory = target_directory, Nproc = Nproc, do_prompt_user = do_prompt_user, petsc_external_packages = petsc_external_packages, mode = mode, flex=flex, bison=bison)
    
    third_party_build.SetMpiCompilers()

    if compiler.blas_library == "Openblas":
        third_party_build.BuildLibrary("Openblas")
    elif compiler.blas_library == "Accelerate":
        pass

    third_party_build.BuildLibrary("Tclap")
    third_party_build.BuildLibrary("Lua")
    third_party_build.BuildLibrary("Libmeshb")
    third_party_build.BuildLibrary("Boost")
    third_party_build.BuildLibrary("Openmpi")
    third_party_build.BuildLibrary("Scotch")
    third_party_build.BuildLibrary("Petsc")

    if do_install_slepc:
        third_party_build.BuildLibrary("Slepc")

    third_party_build.BuildLibrary("Eigen")
    third_party_build.BuildLibrary("Xtl")
    third_party_build.BuildLibrary("Xsimd")
    third_party_build.BuildLibrary("Xtensor")
    third_party_build.BuildLibrary("XtensorBlas")
    
    
    