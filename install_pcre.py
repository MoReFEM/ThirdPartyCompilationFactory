import os
import types
import multiprocessing

from Builder.compiler import Compiler
from Builder.build_third_party_libraries import ThirdPartyBuild


if __name__ == "__main__":
    
    target_directory = "/Users/Shared/LibraryVersions/clang"
    C_compiler = "clang"
    CXX_compiler = "clang++"
    Fortran_compiler = "/usr/local/bin/gfortran"
    blas_library = "Accelerate"
    python3 = "/Users/Shared/Software/Anaconda/bin/python"
    python2 = "/usr/bin/python"
    boost_toolset = "clang"
    do_prompt_user = False
    do_use_fPIC = False
    Nproc = 4 
    
    build_third_party_libraries_directory = os.getcwd()

    compiler = Compiler(C = C_compiler, CXX = CXX_compiler, Fortran = Fortran_compiler, blas_library = blas_library, do_use_fPIC = do_use_fPIC, python2 = python2, python3 = python3,  mex = None, boost_toolset = boost_toolset)
    
    third_party_build = ThirdPartyBuild(compiler, build_third_party_libraries_directory, target_directory, Nproc = Nproc, do_prompt_user = do_prompt_user)
    third_party_build.SetMpiCompilers()
    
    def set_up_pcre(self):
        return self._StandardSetUp("Pcre")
        
    third_party_build.SetUpPcre = types.MethodType(set_up_pcre, third_party_build)
    
    third_party_build.BuildLibrary("Pcre")
