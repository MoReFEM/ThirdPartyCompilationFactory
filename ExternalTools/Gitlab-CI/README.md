# How to set up a gitlab runner?

The steps to set up a runner which will be able to create a Docker image on a CI VM (onto which I suppose gitlab-runner is already installed - I use the same VMs as the ones for CoreLibrary project, and it is documented there how gitlab-runner was installed).

Docker installation instructions stem from https://docs.docker.com/install/linux/docker-ce/ubuntu; they may not be actually required but in any case it's better to use a recent version of Docker.

````
sudo apt update 
sudo apt upgrade

sudo apt-get remove docker docker.io containerd runc

sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
 
sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io   
   
sudo groupadd docker
sudo gpasswd -a $USER docker
newgrp docker
````

The stuff with the Docker group is to enable runs without sudo privileges.

Restart Docker:

````
sudo systemctl restart docker
sudo systemctl enable docker
````

Finally, set up the runner with:

````
sudo gitlab-runner register
````

to which you will provide the token provided [here](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory/-/settings/ci_cd#js-runners-settings).

__IMPORTANT__: make sure to select a _shell_ executor! 



# DNS issue

On CI VM, runnning Docker build didn't work due to DNS issues (the very same that add the requirements to add 
`network_mode="host"` in the config.toml file).

I found how to solve this [here](https://askubuntu.com/questions/233222/how-can-i-disable-the-dns-that-network-manager-uses) thanks to Alexandre Abadie input:

Edit `/etc/NetworkManager/NetworkManager.conf` (with sudo privileges) and comment out the line `dns=dnsmask` with a '#'.

Then restart the network manager with:

````
sudo service network-manager restart
````




