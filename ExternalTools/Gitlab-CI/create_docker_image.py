"""
Create a docker image localy with the rightful tag and deploy it onto Gitlab.

IMPORTANT: This script is expected to be run from the root directory of ThirdPartyCompilationFactory.
"""

import argparse
import os
import subprocess



class build_docker_image(object):
    """
    Build the Docker image and put it in the Gitlab registry
    """
    
    def __init__(self):
        
        parser = argparse.ArgumentParser(
            description='Build the Docker image and put it in the Gitlab registry.',
            formatter_class=argparse.RawDescriptionHelpFormatter)
            
        args = self._interpret_command_line(parser)
        
        tag_latest = ""
        tag_dont_install_external_dependencies = ""

        if not args.install_all_solvers:
            tag_dont_install_external_dependencies = "-no_external_petsc_dependencies"

        registry_image_name = "registry.gitlab.inria.fr/morefem/thirdpartycompilationfactory/{os}-{compiler}-{mode}{tag_dont_install_external_dependencies}".format(mode = args.mode, compiler = args.compiler, os = args.os, tag_dont_install_external_dependencies = tag_dont_install_external_dependencies)

        if args.do_update_latest:
            tag_latest = "-t {}".format(registry_image_name)
        
        cmd = "docker build -f Dockerfile.morefem_third_party.{os}.{compiler} {tag_latest} -t {registry_image_name}:{tag} --build-arg mode={mode} --build-arg compiler={compiler} --build-arg install_all_solvers={install_all_solvers} .".format(mode = args.mode, compiler = args.compiler, os = args.os, tag = args.tag, tag_latest=tag_latest, registry_image_name=registry_image_name, install_all_solvers = args.install_all_solvers)
        
        #print(cmd)
        subprocess.run(cmd, shell=True, check=True)
        

    def _interpret_command_line(self, parser):
        """Interpret the content of the command line.
    
        @param The argparse object.
    
        @return The arguments of the command line.
        """
        
        def check_mode(value):
            if value not in('debug', 'release'):
                raise argparse.ArgumentTypeError("Mode should be either 'debug' or 'release'; you chose \'{}\'.".format(value))
            return value
        
        parser.add_argument(
            '--mode',
            type=check_mode,
            required=True,
            help='Either "debug" or "release".'
        )
        
        def check_os(value):
            if value not in('ubuntu', 'fedora'):
                raise argparse.ArgumentTypeError("OS should be either 'ubuntu' or 'fedora'; you chose \'{}\'.".format(value))
            return value
        
        parser.add_argument(
            '--os',
            type=check_os,
            required=True,            
            help='Either "ubuntu" or "fedora".'
        )
        
        def check_compiler(value):
            if value not in('gcc', 'clang'):
                raise argparse.ArgumentTypeError("Compiler should be either 'gcc' or 'clang'; you chose \'{}\'.".format(value))
            return value
        
        parser.add_argument(
            '--compiler',
            type=check_compiler,
            required=True,            
            help='Either "gcc" or "clang".'
        )
        
        
        parser.add_argument(
            '--tag',
            required=True,            
            help='Tag to use for the Docker images'
        )
        
        
        # Lifted from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
        def str2bool(v):
            if isinstance(v, bool):
               return v
            if v.lower() in ('yes', 'true', 't', 'y', '1'):
                return True
            elif v.lower() in ('no', 'false', 'f', 'n', '0'):
                return False
            else:
                raise argparse.ArgumentTypeError('Boolean value expected.')
        
        parser.add_argument(
            '--do_update_latest',
            type=str2bool,
            required=True,
            help='If true, "latest" tag will be given to the generated Docker image'
        )


        parser.add_argument(
            '--install_all_solvers',
            type=str2bool,
            required=True,
            help='Tells whether all solvers supported by MoReFEM must be installed along with PETSc. In most installations should be true, but I need at least one with false to check MoReFEM behaves properly in this case. At the moment SuperLU_dist is always installed no matter what as it acts as default solver.')
                                
    
        args = parser.parse_args()
    
        return args


if __name__ == "__main__":
    
    build_docker_image()





