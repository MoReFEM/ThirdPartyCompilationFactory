import argparse
import multiprocessing

from Builder.docker_build import docker_build


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description = 'Call build of third party libraries in a Docker environment.')
    
    parser.add_argument('--mode',
                        help='Specify the mode among "debug", "release" and "debug_asan" (the latter to use address sanitizer in the compilation)')
                        
    parser.add_argument('--compiler',
                        help='Specify the compiler among "gcc" and "clang". The chosen compiler suite will be used for C and C++; gfortran is used in any case for Fortran.')
    
    # Lifted from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    def str2bool(v):
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')
   
    parser.add_argument(
            '--install_all_solvers',
            type=str2bool,
            required=True,
            help='Tells whether all solvers supported by MoReFEM must be installed along with PETSc. In most installations should be true, but I need at least one with false to check MoReFEM behaves properly in this case. At the moment SuperLU_dist is always installed no matter what as it acts as default solver.')
                                
                        
                        
    args = parser.parse_args()
    
    if args.compiler not in ("gcc", "clang"):
        raise Exception("Compiler suite must be either 'gcc' or 'clang'; you chose '{}'".format(args.compiler))
        
    
    target_directory = "/opt"
    Fortran_compiler = "gfortran"
    blas_library = "Openblas"
    python = "python3"
    
    if args.compiler == "gcc":
        C_compiler = "gcc"
        CXX_compiler = "g++"
        boost_toolset = "gcc"
    else:
        C_compiler = "clang"
        CXX_compiler = "clang++"
        boost_toolset = "clang"

    
        
    
    docker_build(target_directory = target_directory, \
                 C_compiler = C_compiler, \
                 CXX_compiler = CXX_compiler, \
                 python = python, \
                 Fortran_compiler = Fortran_compiler, \
                 blas_library = blas_library, \
                 do_use_fPIC = True, \
                 Nproc = multiprocessing.cpu_count(), \
                 mode = args.mode, \
                 do_install_all_supported_petsc_solvers = args.install_all_solvers, \
                 boost_toolset = boost_toolset)

