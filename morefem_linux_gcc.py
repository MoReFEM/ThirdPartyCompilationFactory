import multiprocessing

import Builder.install_morefem_libraries


if __name__ == "__main__":
    
    do_prompt_user = False
    
    mode = "release"
    target_directory = "/opt"
    C_compiler = "gcc"
    CXX_compiler = "g++"
    Fortran_compiler = "gfortran"
    blas_library = "Openblas"
    python = "python"
    boost_toolset = "gcc"
    do_install_slepc=True
    
    Builder.install_morefem_libraries.install_morefem_libraries(target_directory = target_directory, \
                                                                  C_compiler = C_compiler, \
                                                                  CXX_compiler = CXX_compiler, \
                                                                  python = python, \
                                                                  Fortran_compiler = Fortran_compiler, \
                                                                  mode = mode, \
                                                                  blas_library = blas_library, \
                                                                  do_use_fPIC = True, \
                                                                  Nproc = multiprocessing.cpu_count(), \
                                                                  boost_toolset = boost_toolset,
                                                                  do_prompt_user = do_prompt_user,do_install_slepc=do_install_slepc)