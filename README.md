# Introduction

This project is just a facility to install seamlessly libraries that are commonly used within the M3DISIM team.

For more advanced developers, this enables installing easily the same libraries compiled with different compilers or flavor of compilers (for instance on my machine I compile them with Apple clang, gcc and a clang compiled from llvm sources) and options (Blas may for instance be provided by the framework Accelerate on a Mac or by Openblas).

To use it properly, you have few 'universal' prerequisites:

- CMake
- Fortran (gfortran in practice so far).
- Module 'distro' installed for Python; use `python -m pip install distro`

For Linux distributions, you also have to make sure to install `libreadline` and `libncurses` for Lua's sake (have a look at Dockerfiles to find out the name of the packages in Ubuntu and Fedora).

Each installed libraries will come with a _M3DISIM\_Build.txt_ file which will describe when and how it was compiled; for each library a script named _m3disim\_build\_Library name_ is created.

# Direct installation

You may want to install it directly on your machine; in this case you should consider the Python scripts named `morefem_*`, e.g. `morefem_macos_clang.py`.

The idea here is to copy the script closest to what you wish (choose the same distribution), edit the copy and fill with the settings you want.

Then run in command line with Python:

````
python3 morefem_macos_clang.py
````

An option deserves to be highlighted: `do_prompt_user` allows to ask the user whether each library must be installed ot not. It is handy if one fails: you do not want to reinstall everything from scratch in this case.


# Docker

Three Dockerfiles are provided: one for Ubuntu and two for Fedora (one for gcc and the other for clang).

They might seem a bit convoluted, but they have been built with one goal in mind: we want to be able to know immediately if something goes awry somewhere. For instance Dockerfile is written so that the RUN command that fail might be immediately identified (that was not the case in former versions in which a single Python script did all the installation work).

__WARNING__: For the Dockerfile to succeed, you have to make sure to allocate enough memory to Docker! I spent hours trying to understand the internal compiler error obtained during Petsc compilation, and it was in fact a lack of memory (the default 2Go allocated to Docker were simply not enough).

There is now a facility to build Docker image directly from the Gitlab web interface; to use it:

- Make sure the branch for which you want to Docker images are properly present in Gitlab.
- Go to the [CI/CD page](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory/pipelines) in the Gilab project and choose _Run pipeline_.
- Select in the _Run for_ the branch from which the Docker images must be generated.
- Add a variable named __TAG__ and associate the name of the tag to use (e.g. _v20.10_).
- Add a variable named __UPDATE_LATEST_TAG__ and associate _True_ or _False_, depending on whether you want to overwrite the _latest_ tag of the Docker images.



