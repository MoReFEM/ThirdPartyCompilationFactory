import multiprocessing
import Builder.install_morefem_libraries


if __name__ == "__main__":
    
    mode = 'release'
    target_directory = "/Volumes/Data/opt/gcc_{}".format(mode)
    C_compiler = "/usr/local/bin/gcc"
    CXX_compiler = "/usr/local/bin/g++"
    Fortran_compiler = "/usr/local/bin/gfortran"
    blas_library = "Openblas"
    python = "/Users/Shared/Software/Anaconda/bin/python"
    boost_toolset = "gcc"
    do_prompt_user = True
    do_install_slepc = True

    # Flex and Bison values here are for ARM macOS when they were installed with Homebrew.
    flex = "/opt/homebrew/opt/flex/bin/flex"
    bison = "/opt/homebrew/opt/bison/bin/bison"

    
    Builder.install_morefem_libraries.install_morefem_libraries(target_directory = target_directory, \
                                                                  C_compiler = C_compiler, \
                                                                  CXX_compiler = CXX_compiler, \
                                                                  python = python, \
                                                                  Fortran_compiler = Fortran_compiler, \
                                                                  mode = mode, \
                                                                  blas_library = blas_library, \
                                                                  do_use_fPIC = False, \
                                                                  Nproc = multiprocessing.cpu_count(), \
                                                                  boost_toolset = boost_toolset,
                                                                  do_prompt_user = do_prompt_user,
                                                                  do_install_slepc=do_install_slepc,
                                                                  flex = flex,
                                                                  bison = bison)