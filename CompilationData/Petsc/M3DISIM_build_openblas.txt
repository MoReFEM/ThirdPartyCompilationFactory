Petsc v3.22.2

http://www.mcs.anl.gov/petsc

Compiled the [M3DISIM_INSTALL_DATE] with:

[M3DISIM_INSTALL_COMPILER_INFO]


Due to its importance in the code and the many ways it can go awry, Petsc is compiled both in debug and release mode.

Prerequisite:
	- OpenMPI.

Procedure:


$ export PETSC_ARCH=config_morefem
$ [M3DISIM_INSTALL_PYTHON] config/configure.py CXXFLAGS="${CXXFLAGS} [M3DISIM_INSTALL_FPIC] -m64 [M3DISIM_INSTALL_MODE_FLAGS] [M3DISIM_INSTALL_SANITIZER_FLAGS] [M3DISIM_INSTALL_STLLIB]" CFLAGS="${CFLAGS} [M3DISIM_INSTALL_FPIC] -m64 [M3DISIM_INSTALL_MODE_FLAGS] [M3DISIM_INSTALL_SANITIZER_FLAGS]" FFLAGS="${FFLAGS} [M3DISIM_INSTALL_FPIC] [M3DISIM_INSTALL_MODE_FLAGS] [M3DISIM_INSTALL_SANITIZER_FLAGS]" FCFLAGS="${FCFLAGS} [M3DISIM_INSTALL_FPIC] [M3DISIM_INSTALL_MODE_FLAGS] [M3DISIM_INSTALL_SANITIZER_FLAGS]" LDFLAGS="${LDFLAGS} [M3DISIM_INSTALL_FPIC] [M3DISIM_INSTALL_GOMP]" --with-mpi-dir=[M3DISIM_INSTALL_LIBRARY_DIRECTORY]/Openmpi --download-cmake=https://github.com/Kitware/CMake/releases/download/v3.30.3/cmake-3.30.3.tar.gz [M3DISIM_PETSC_DOWNLOAD_MUMPS] [M3DISIM_PETSC_DOWNLOAD_SUPERLU_DIST] [M3DISIM_PETSC_DOWNLOAD_UMFPACK] [M3DISIM_PETSC_DOWNLOAD_SCALAPACK]  --with-clib-autodetect=0  --with-debugging=[M3DISIM_PETSC_IS_DEBUGGING] --with-blas-lapack-include=[M3DISIM_INSTALL_LIBRARY_DIRECTORY]/Openblas   --download-sowing-cc=[M3DISIM_INSTALL_C_COMPILER] ---download-sowing-cxx=[M3DISIM_INSTALL_CXX_COMPILER] --with-cxx-dialect=C++17 --with-blas-lapack-include=[M3DISIM_INSTALL_LIBRARY_DIRECTORY]/Openblas  --with-blas-lapack-lib="[M3DISIM_INSTALL_BLAS]"

$ make all
